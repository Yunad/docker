package com.example.docker;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@SpringBootApplication
public class DockerApplication {

    @Value("${test_variable}")
    private String test_variable;

    @Value("${node_name}")
    private String node_name;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootAppApplication.class, args);
    }

    private String getContainerId() {
        String[] cmd = { "/bin/sh", "-c", "cat /etc/hostname" };
        String output = null;
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(cmd);
            InputStream stdout = process.getInputStream();
            BufferedReader reader = new BufferedReader (new InputStreamReader(stdout));
            output = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return output;
    }

    @GetMapping("/")
    @ResponseBody
    public String index() {
        return "Node Name: " + node_name + "</br>Container ID: " + getContainerId() + "</br>TEST_VARIABLE: " + test_variable;
    }
}