FROM openjdk:11.0.10-jdk

ENV TEST_VARIABLE=default_value

RUN mkdir app
COPY target/docker-1.0.jar /opt/app.jar

ENTRYPOINT ["java","-jar","/opt/app.jar"]